#!/bin/env python
# -*- coding: utf-8 -*-
from __future__ import print_function
import io
import sys
import json
import random

TYPE = ['Διαμέρισμα', 'Μονοκατοικία', 'Γκαρσονιέρα', 'Μεζονέτα', 'Οροφοδιαμέρισμα']
DESCRIPTION = ['Ηλιόλουστο σπίτι, χωρίς σκεπή.', 'Σπίτι με θέα Καμάρα και Παρθενώνα']
AREA = [20, 150]
PRICE = [15000, 500000]
PRICE_PER_SQM = [1000, 2500]
FLOORS = ['', 'Ισόγειο', 'Υπόγειο', '1', '2', '3', '4']
BEDROOMS = [0, 4]
BATHROOMS = [0, 3]
WC = [0, 2]
PARKING = ['', 'Ναι', 'Όχι']
HEATING = ['', 'Φυσικό Αέριο', 'Πετρέλαιο', 'Μαζουτ', 'Πέλλετ', 'Κάρβουνο']
LOCATION = ['', 'Τούμπα', 'Χαριλάου', 'Μαρτίου', 'Συκιές', 'Ρετζίκι', 'Πλαγιάρι']
ADDRESS = ['Παπανδρέου ', 'Εγνατίας ', 'Τσιμισκή ', 'Πανόρμου ', 'Λαμπράκη ']
CONSTRUCTION_YEAR = [1950, 2019]
RENOVATION_YEAR = [1990, 2019]
COORD_BASE_LAT = 40.605804
COORD_BASE_LON = 22.973337
COORD_EPSILON = 0.02
COORD_TYPE = ['', 'Ναί', 'Όχι', 'Άγνωστο']
IMAGES = [
    'https://imgur.com/gWIGFrZ.jpg',
    'https://imgur.com/Ixmjoz8.jpg',
    'https://imgur.com/vzTJg18.jpg',
    'https://imgur.com/O3TVPiF.jpg',
    'https://imgur.com/ymhN3vP.jpg',
    'https://imgur.com/hbeHhek.jpg',
    'https://imgur.com/Cm12brH.jpg',
    'https://imgur.com/YE5dVuy.jpg',
    'https://imgur.com/NKs7qbe.jpg',
    'https://imgur.com/Xl3AxSK.jpg',
    'https://imgur.com/kHIfCml.jpg',
    'https://imgur.com/NJtG4zb.jpg',
    'https://imgur.com/y9q9MOm.jpg',
    'https://imgur.com/YhEX2as.jpg',
    'https://imgur.com/LjtxGQI.jpg',
    'https://imgur.com/FBM7ZXX.jpg',
    'https://imgur.com/YQ7DVoy.jpg',
    'https://imgur.com/hK8ELid.jpg',
    'https://imgur.com/RWiazKh.jpg',
    'https://imgur.com/SAZXBAh.jpg',
    'https://imgur.com/eOs81WO.jpg',
    'https://imgur.com/JGFqoNA.jpg',
    'https://imgur.com/kuuM3My.jpg',
    'https://imgur.com/tDbBVyS.jpg',
    'https://imgur.com/ODic7Bq.jpg',
]
NUM_PAGES = 5
HOMES_PER_PAGE = 20

def create_site():
    for i in range(NUM_PAGES):
        create_page(i)

HEADER = """<!DOCTYPE html>
<html>
<head>
    <title>{title}</title>
    <link href="style.css" rel="stylesheet">
</head>
<body>"""

FOOTER = """
</body>
</html>"""

def create_page(page_number):
    stdout_backup = sys.stdout
    sys.stdout = io.open('page_%d.html' % (page_number + 1), mode='w', encoding='utf-8')

    print(HEADER.format(title="MyRealEstate"))

    # Home links
    print("""<div id=listings>
    <table>""")

    for i in range(HOMES_PER_PAGE):
        home_num = page_number * HOMES_PER_PAGE + i + 1
        print("        <tr><td><a href='home_{home_num}.html'>Home {home_num}</a></td></tr>".format(home_num=home_num))
        create_home(home_num)

    print("""    </table>
</div>
<br>""")

    # Pagination
    print("""<div id=pagination>
    Page:""")

    for i in range(NUM_PAGES):
        if i == page_number:
            print("    {page_num}".format(page_num=i + 1))
        else:
            print("    <a href='page_{page_num}.html'>{page_num}</a>".format(page_num=i + 1))

    print("</div>")

    print(FOOTER)

    sys.stdout = stdout_backup


def create_home(home_number):
    home = {
        'Περιγραφή': random.choice(DESCRIPTION),
        'Τύπος': random.choice(TYPE),
        'Τετραγωνικά': random.randint(AREA[0], AREA[1]),
        'Τιμή': random.randint(PRICE[0], PRICE[1]),
        'Τιμή/τ.μ.': random.randint(PRICE_PER_SQM[0], PRICE_PER_SQM[1]),
        'Όροφος': random.choice(FLOORS),
        'Κρεβατοκάμαρες': random.randint(BEDROOMS[0], BEDROOMS[1]),
        'Μπάνια': random.randint(BATHROOMS[0], BATHROOMS[1]),
        'WC': random.randint(WC[0], WC[1]),
        'Θέρμανση': random.choice(HEATING),
        'Έτος κατασκευής': random.randint(CONSTRUCTION_YEAR[0], CONSTRUCTION_YEAR[1]),
        'Έτος ανακαίνισης': random.randint(RENOVATION_YEAR[0], RENOVATION_YEAR[1]),
        'Parking': random.choice(PARKING),
        'Περιοχή': random.choice(LOCATION),
        'Διεύθυνση': random.choice(ADDRESS) + str(random.randint(1, 200)),
        'Lat': random.uniform(COORD_BASE_LAT - COORD_EPSILON, COORD_BASE_LAT + COORD_EPSILON),
        'Lon': random.uniform(COORD_BASE_LON - COORD_EPSILON, COORD_BASE_LON + COORD_EPSILON),
        'Ακριβής τοποθεσία': random.choice(COORD_TYPE),
        'Ημερομηνία δημοσίευσης': '5/3/%d' % random.randint(2015, 2018),
        'Ημερομηνία ενημέρωσης': '7/9/%d' % random.randint(2018, 2021),
    }
    title = random.choice(TYPE) + ' προς πώληση - ' + str(random.randint(PRICE[0], PRICE[1])) + ' ευρά'
    images = [ random.choice(IMAGES) ]

    stdout_backup = sys.stdout
    sys.stdout = io.open('home_%d.html' % (home_number), mode='w', encoding='utf-8')

    print(HEADER.format(title=title))

    print("""<div id=home>
    <table>""")

    print("    <tr><td colspan=2><center><img src='{image}'></center></td></tr>".format(image=images[0]))
    for key in home:
        value = home[key]
        print("        <tr><td id=info>{key}:</td><td>{value}</td></tr>".format(
            key=key, value=value))

    print("""</table>
</div>""")

    print(FOOTER)

    sys.stdout = stdout_backup

if __name__ == '__main__':
    create_site()
